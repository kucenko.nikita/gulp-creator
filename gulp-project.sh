#!/bin/bash

futureFolder=$1


if ! [ -d $futureFolder ]; then
    echo 'No directory'
elif [ -z "$futureFolder" ]; then
    echo 'Folder not written'
else
    cp /home/ostperino/bashes/gulp-creator/gulpfile.js $futureFolder

    cd $futureFolder

    mkdir -p src/{scss,js} && touch src/{index.pug,js/script.js,scss/style.scss}


    npm init

    npm install --save-dev gulp
    npm install --save-dev browser-sync
    npm install --save-dev gulp-pug
    npm install --save-dev gulp-sass
    npm install --save-dev sass
    npm install --save-dev gulp-autoprefixer
    npm install --save-dev gulp-typescript

fi